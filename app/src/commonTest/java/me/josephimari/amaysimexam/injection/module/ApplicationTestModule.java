package me.josephimari.amaysimexam.injection.module;

import android.accounts.AccountManager;
import android.app.Application;
import android.content.Context;

import me.josephimari.amaysimexam.data.DataManager;
import me.josephimari.amaysimexam.data.remote.AmaysimExamService;
import me.josephimari.amaysimexam.injection.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

import static org.mockito.Mockito.mock;

/**
 * Provides application-level dependencies for an app running on a testing environment
 * This allows injecting mocks if necessary.
 */
@Module
public class ApplicationTestModule {
    protected final Application mApplication;

    public ApplicationTestModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    /************* MOCKS *************/

    @Provides
    @Singleton
    DataManager providesDataManager() {
        return mock(DataManager.class);
    }

    @Provides
    @Singleton
    AmaysimExamService provideRibotService() {
        return mock(AmaysimExamService.class);
    }

    @Provides
    @Singleton
    AccountManager provideAccountManager() {
        return mock(AccountManager.class);
    }
}
