package me.josephimari.amaysimexam.injection.component;

import javax.inject.Singleton;

import dagger.Component;
import me.josephimari.amaysimexam.injection.module.ApplicationTestModule;

@Singleton
@Component(modules = ApplicationTestModule.class)
public interface TestComponent extends ApplicationComponent {

}
