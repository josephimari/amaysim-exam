package me.josephimari.amaysimexam;

import android.app.Application;
import android.content.Context;

import me.josephimari.amaysimexam.injection.component.ApplicationComponent;
import me.josephimari.amaysimexam.injection.component.DaggerApplicationComponent;
import me.josephimari.amaysimexam.injection.module.ApplicationModule;

import timber.log.Timber;

public class AmaysimExamApplication extends Application {

    ApplicationComponent mApplicationComponent;

    @Override
    public void onCreate() {
        super.onCreate();

        if (BuildConfig.DEBUG) Timber.plant(new Timber.DebugTree());

        mApplicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();
        mApplicationComponent.inject(this);
    }

    public static AmaysimExamApplication get(Context context) {
        return (AmaysimExamApplication) context.getApplicationContext();
    }

    public ApplicationComponent getComponent() {
        return mApplicationComponent;
    }

    // Needed to replace the component with a test specific one
    public void setComponent(ApplicationComponent applicationComponent) {
        mApplicationComponent = applicationComponent;
    }
}
