package me.josephimari.amaysimexam.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Joseph on 1/30/2017.
 */

public class Included implements Parcelable {

    public String type;
    public String id;
    public Attribute attributes;
    public Link links;
    public Relationship relationships;

    public Included() {
    }

    protected Included(Parcel in) {
        type = in.readString();
        id = in.readString();
        attributes = (Attribute) in.readValue(Attribute.class.getClassLoader());
        links = (Link) in.readValue(Link.class.getClassLoader());
        relationships = (Relationship) in.readValue(Relationship.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(type);
        dest.writeString(id);
        dest.writeValue(attributes);
        dest.writeValue(links);
        dest.writeValue(relationships);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Included> CREATOR = new Parcelable.Creator<Included>() {
        @Override
        public Included createFromParcel(Parcel in) {
            return new Included(in);
        }

        @Override
        public Included[] newArray(int size) {
            return new Included[size];
        }
    };

    @Override
    public String toString() {
        return "Included{" +
                "type='" + type + '\'' +
                ", id='" + id + '\'' +
                ", attributes=" + attributes +
                ", links=" + links +
                ", relationships=" + relationships +
                '}';
    }
}
