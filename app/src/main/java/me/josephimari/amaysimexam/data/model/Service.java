package me.josephimari.amaysimexam.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Joseph on 1/30/2017.
 */

public class Service implements Parcelable {

    public Link links;

    public Service() {
    }

    protected Service(Parcel in) {
        links = (Link) in.readValue(Link.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(links);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Service> CREATOR = new Parcelable.Creator<Service>() {
        @Override
        public Service createFromParcel(Parcel in) {
            return new Service(in);
        }

        @Override
        public Service[] newArray(int size) {
            return new Service[size];
        }
    };

    @Override
    public String toString() {
        return "Service{" +
                "links=" + links +
                '}';
    }
}
