package me.josephimari.amaysimexam.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Joseph on 1/30/2017.
 */
public class Attribute implements Parcelable {

    //type: accounts
    @SerializedName("payment-type")
    public String paymentType;
    @SerializedName("unbilled-charges")
    public Double unbilledCharges;
    @SerializedName("next-billing-date")
    public String nextBillingDate;
    public String title;
    @SerializedName("first-name")
    public String firstName;
    @SerializedName("last-name")
    public String lastName;
    @SerializedName("date-of-birth")
    public String dateOfBirth;
    @SerializedName("contact-number")
    public  String contactNumber;
    @SerializedName("email-address")
    public String emailAddress;
    @SerializedName("email-address-verified")
    public boolean emailAddressVerified;
    @SerializedName("email-subscription-status")
    public boolean emailSubscriptionStatus;

    //type: services
    public String msn;
    public Long credit;
    @SerializedName("credit-expiry")
    public String creditExpiry;
    @SerializedName("data-usage-threshold")
    public boolean dataUsageThreshold;

    //type: subscriptions
    @SerializedName("included-data-balance")
    public Long includedDataBalance;
    @SerializedName("included-credit-balance")
    public Long includedCreditBalance;
    @SerializedName("included-rollover-data-balance")
    public Long includedRolloverDataBalance;
    @SerializedName("included-rollover-credit-balance")
    public Long includedRolloverCreditBalance;
    @SerializedName("included-international-talk-balance")
    public Long includedInternationalTalkBalance;
    @SerializedName("expiry-date")
    public String expiryDate;
    @SerializedName("auto-renewal")
    public boolean autoRenewal;
    @SerializedName("primary-subscription")
    public boolean primarySubscription;

    //type: product
    public String name;
    @SerializedName("included-data")
    public Long includedData;
    @SerializedName("included-credit")
    public Long includedCredit;
    @SerializedName("included-international-talk")
    public Long includedInternationTalk;
    @SerializedName("unlimited-text")
    public boolean unlimitedText;
    @SerializedName("unlimited-talk")
    public boolean unlimitedTalk;
    @SerializedName("unlimited-international-text")
    public boolean unlimitedInternationalText;
    @SerializedName("unlimited-international-talk")
    public boolean unlimitedInternationalTalk;
    public Double price;

    public Attribute() {

    }

    protected Attribute(Parcel in) {
        paymentType = in.readString();
        unbilledCharges = in.readByte() == 0x00 ? null : in.readDouble();
        nextBillingDate = in.readString();
        title = in.readString();
        firstName = in.readString();
        lastName = in.readString();
        dateOfBirth = in.readString();
        contactNumber = in.readString();
        emailAddress = in.readString();
        emailAddressVerified = in.readByte() != 0x00;
        emailSubscriptionStatus = in.readByte() != 0x00;
        msn = in.readString();
        credit = in.readByte() == 0x00 ? null : in.readLong();
        creditExpiry = in.readString();
        dataUsageThreshold = in.readByte() != 0x00;
        includedDataBalance = in.readByte() == 0x00 ? null : in.readLong();
        includedCreditBalance = in.readByte() == 0x00 ? null : in.readLong();
        includedRolloverDataBalance = in.readByte() == 0x00 ? null : in.readLong();
        includedRolloverCreditBalance = in.readByte() == 0x00 ? null : in.readLong();
        includedInternationalTalkBalance = in.readByte() == 0x00 ? null : in.readLong();
        expiryDate = in.readString();
        autoRenewal = in.readByte() != 0x00;
        primarySubscription = in.readByte() != 0x00;
        name = in.readString();
        includedData = in.readByte() == 0x00 ? null : in.readLong();
        includedCredit = in.readByte() == 0x00 ? null : in.readLong();
        includedInternationTalk = in.readByte() == 0x00 ? null : in.readLong();
        unlimitedText = in.readByte() != 0x00;
        unlimitedTalk = in.readByte() != 0x00;
        unlimitedInternationalText = in.readByte() != 0x00;
        unlimitedInternationalTalk = in.readByte() != 0x00;
        price = in.readByte() == 0x00 ? null : in.readDouble();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(paymentType);
        if (unbilledCharges == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(unbilledCharges);
        }
        dest.writeString(nextBillingDate);
        dest.writeString(title);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(dateOfBirth);
        dest.writeString(contactNumber);
        dest.writeString(emailAddress);
        dest.writeByte((byte) (emailAddressVerified ? 0x01 : 0x00));
        dest.writeByte((byte) (emailSubscriptionStatus ? 0x01 : 0x00));
        dest.writeString(msn);
        if (credit == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(credit);
        }
        dest.writeString(creditExpiry);
        dest.writeByte((byte) (dataUsageThreshold ? 0x01 : 0x00));
        if (includedDataBalance == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(includedDataBalance);
        }
        if (includedCreditBalance == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(includedCreditBalance);
        }
        if (includedRolloverDataBalance == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(includedRolloverDataBalance);
        }
        if (includedRolloverCreditBalance == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(includedRolloverCreditBalance);
        }
        if (includedInternationalTalkBalance == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(includedInternationalTalkBalance);
        }
        dest.writeString(expiryDate);
        dest.writeByte((byte) (autoRenewal ? 0x01 : 0x00));
        dest.writeByte((byte) (primarySubscription ? 0x01 : 0x00));
        dest.writeString(name);
        if (includedData == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(includedData);
        }
        if (includedCredit == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(includedCredit);
        }
        if (includedInternationTalk == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeLong(includedInternationTalk);
        }
        dest.writeByte((byte) (unlimitedText ? 0x01 : 0x00));
        dest.writeByte((byte) (unlimitedTalk ? 0x01 : 0x00));
        dest.writeByte((byte) (unlimitedInternationalText ? 0x01 : 0x00));
        dest.writeByte((byte) (unlimitedInternationalTalk ? 0x01 : 0x00));
        if (price == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeDouble(price);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Attribute> CREATOR = new Parcelable.Creator<Attribute>() {
        @Override
        public Attribute createFromParcel(Parcel in) {
            return new Attribute(in);
        }

        @Override
        public Attribute[] newArray(int size) {
            return new Attribute[size];
        }
    };

    @Override
    public String toString() {
        return "Attribute{" +
                "paymentType='" + paymentType + '\'' +
                ", unbilledCharges=" + unbilledCharges +
                ", nextBillingDate='" + nextBillingDate + '\'' +
                ", title='" + title + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", contactNumber='" + contactNumber + '\'' +
                ", emailAddress='" + emailAddress + '\'' +
                ", emailAddressVerified=" + emailAddressVerified +
                ", emailSubscriptionStatus=" + emailSubscriptionStatus +
                ", msn='" + msn + '\'' +
                ", credit=" + credit +
                ", creditExpiry='" + creditExpiry + '\'' +
                ", dataUsageThreshold=" + dataUsageThreshold +
                ", includedDataBalance=" + includedDataBalance +
                ", includedCreditBalance=" + includedCreditBalance +
                ", includedRolloverDataBalance=" + includedRolloverDataBalance +
                ", includedRolloverCreditBalance=" + includedRolloverCreditBalance +
                ", includedInternationalTalkBalance=" + includedInternationalTalkBalance +
                ", expiryDate='" + expiryDate + '\'' +
                ", autoRenewal=" + autoRenewal +
                ", primarySubscription=" + primarySubscription +
                ", name='" + name + '\'' +
                ", includedData=" + includedData +
                ", includedCredit=" + includedCredit +
                ", includedInternationTalk=" + includedInternationTalk +
                ", unlimitedText=" + unlimitedText +
                ", unlimitedTalk=" + unlimitedTalk +
                ", unlimitedInternationalText=" + unlimitedInternationalText +
                ", unlimitedInternationalTalk=" + unlimitedInternationalTalk +
                ", price=" + price +
                '}';
    }
}