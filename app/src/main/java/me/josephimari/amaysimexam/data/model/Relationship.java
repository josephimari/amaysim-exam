package me.josephimari.amaysimexam.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Joseph on 1/30/2017.
 */

public class Relationship implements Parcelable {

    public Service services;
    public Subscription subscriptions;
    public Product product;
    public Downgrade downgrade;

    public Relationship() {
    }

    protected Relationship(Parcel in) {
        services = (Service) in.readValue(Service.class.getClassLoader());
        subscriptions = (Subscription) in.readValue(Subscription.class.getClassLoader());
        product = (Product) in.readValue(Product.class.getClassLoader());
        downgrade = (Downgrade) in.readValue(Downgrade.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(services);
        dest.writeValue(subscriptions);
        dest.writeValue(product);
        dest.writeValue(downgrade);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Relationship> CREATOR = new Parcelable.Creator<Relationship>() {
        @Override
        public Relationship createFromParcel(Parcel in) {
            return new Relationship(in);
        }

        @Override
        public Relationship[] newArray(int size) {
            return new Relationship[size];
        }
    };

    @Override
    public String toString() {
        return "Relationship{" +
                "services=" + services +
                ", subscriptions=" + subscriptions +
                ", product=" + product +
                ", downgrade=" + downgrade +
                '}';
    }
}
