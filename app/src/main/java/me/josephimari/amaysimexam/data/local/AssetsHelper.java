package me.josephimari.amaysimexam.data.local;

import android.content.Context;

import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import me.josephimari.amaysimexam.data.model.Collection;
import me.josephimari.amaysimexam.data.model.Datum;
import me.josephimari.amaysimexam.data.model.Included;
import me.josephimari.amaysimexam.injection.ApplicationContext;

/**
 * Created by Joseph on 1/30/2017.
 */

public class AssetsHelper {

    public static final String COLLECTION_FILE_NAME = "collection.json";
    private String collectionJsonString;

    @Inject
    public AssetsHelper(@ApplicationContext Context context) {
        try {
            collectionJsonString = readFromAssets(context, COLLECTION_FILE_NAME);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static String readFromAssets(Context context, String filename) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(context.getAssets().open(filename)));

        // do reading, usually loop until end of file reading
        StringBuilder sb = new StringBuilder();
        String mLine = reader.readLine();
        while (mLine != null) {
            sb.append(mLine); // process line
            mLine = reader.readLine();
        }
        reader.close();
        return sb.toString();
    }

    public Collection getCollection() {
        return new Gson().fromJson(collectionJsonString, Collection.class);
    }

    public String getMsn() {

        Collection collection = getCollection();
        if (collection == null) return null;

        List<Included> includedList = collection.included;
        if (includedList == null || includedList.size() == 0) return null;

        for (Included included : includedList) {
            if (included.type != null && included.type.equals("services")) {
                if (included.attributes != null) {
                    return included.attributes.msn;
                }
            }
        }

        return null;
    }

    public String getSalutation() {
        return getTitle() + " " + getFirstName() + " " + getLastName();
    }

    public String getTitle() {
        Collection collection = getCollection();
        if (collection == null) return null;

        Datum datum = collection.data;
        if (datum == null || datum.attributes == null) return "";

        String title = datum.attributes.title;
        return title == null ? "" : title;
    }

    public String getFirstName() {
        Collection collection = getCollection();
        if (collection == null) return null;

        Datum datum = collection.data;
        if (datum == null || datum.attributes == null) return "";

        String firstName = datum.attributes.firstName;
        return firstName == null ? "" : firstName;
    }

    public String getLastName() {
        Collection collection = getCollection();
        if (collection == null) return null;

        Datum datum = collection.data;
        if (datum == null || datum.attributes == null) return "";

        String lastName = datum.attributes.lastName;
        return lastName == null ? "" : lastName;
    }

    public List<Included> getIncludedList() {
        Collection collection = getCollection();
        if (collection == null) return null;

        return collection.included == null ? new ArrayList<Included>(0) : collection.included;
    }

    public Datum getAccount() {
        Collection collection = getCollection();
        if (collection == null) return null;

        return collection.data;
    }
}
