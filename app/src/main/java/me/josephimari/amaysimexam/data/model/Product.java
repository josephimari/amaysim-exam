package me.josephimari.amaysimexam.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Joseph on 1/30/2017.
 */

public class Product implements Parcelable {

    public Datum data;

    public Product() {
    }

    protected Product(Parcel in) {
        data = (Datum) in.readValue(Datum.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(data);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Product> CREATOR = new Parcelable.Creator<Product>() {
        @Override
        public Product createFromParcel(Parcel in) {
            return new Product(in);
        }

        @Override
        public Product[] newArray(int size) {
            return new Product[size];
        }
    };

    @Override
    public String toString() {
        return "Product{" +
                "data=" + data +
                '}';
    }
}
