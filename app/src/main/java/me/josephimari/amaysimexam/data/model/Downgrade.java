package me.josephimari.amaysimexam.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Joseph on 1/30/2017.
 */

public class Downgrade implements Parcelable {

    public Datum data;

    public Downgrade() {
    }

    protected Downgrade(Parcel in) {
        data = (Datum) in.readValue(Datum.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(data);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Downgrade> CREATOR = new Parcelable.Creator<Downgrade>() {
        @Override
        public Downgrade createFromParcel(Parcel in) {
            return new Downgrade(in);
        }

        @Override
        public Downgrade[] newArray(int size) {
            return new Downgrade[size];
        }
    };

    @Override
    public String toString() {
        return "Downgrade{" +
                "data=" + data +
                '}';
    }
}
