package me.josephimari.amaysimexam.data.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Joseph on 1/30/2017.
 */

public class Link implements Parcelable {

    public String self;
    public String related;

    public Link() {
    }

    protected Link(Parcel in) {
        self = in.readString();
        related = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(self);
        dest.writeString(related);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Link> CREATOR = new Parcelable.Creator<Link>() {
        @Override
        public Link createFromParcel(Parcel in) {
            return new Link(in);
        }

        @Override
        public Link[] newArray(int size) {
            return new Link[size];
        }
    };

    @Override
    public String toString() {
        return "Link{" +
                "self='" + self + '\'' +
                ", related='" + related + '\'' +
                '}';
    }
}
