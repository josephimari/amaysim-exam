package me.josephimari.amaysimexam.data.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Joseph on 1/30/2017.
 */
public class Collection implements Parcelable {

    public Datum data;
    public List<Included> included;

    public Collection() {

    }

    protected Collection(Parcel in) {
        data = (Datum) in.readValue(Datum.class.getClassLoader());
        if (in.readByte() == 0x01) {
            included = new ArrayList<Included>();
            in.readList(included, Included.class.getClassLoader());
        } else {
            included = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(data);
        if (included == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(included);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Collection> CREATOR = new Parcelable.Creator<Collection>() {
        @Override
        public Collection createFromParcel(Parcel in) {
            return new Collection(in);
        }

        @Override
        public Collection[] newArray(int size) {
            return new Collection[size];
        }
    };

    @Override
    public String toString() {
        return "Collection{" +
                "data=" + data +
                ", included=" + included +
                '}';
    }
}
