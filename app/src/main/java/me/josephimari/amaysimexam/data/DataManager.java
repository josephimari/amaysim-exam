package me.josephimari.amaysimexam.data;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import me.josephimari.amaysimexam.data.local.AssetsHelper;
import me.josephimari.amaysimexam.data.local.PreferencesHelper;
import me.josephimari.amaysimexam.data.model.Character;
import me.josephimari.amaysimexam.data.model.Collection;
import me.josephimari.amaysimexam.data.model.Datum;
import me.josephimari.amaysimexam.data.model.Included;
import me.josephimari.amaysimexam.data.remote.AmaysimExamService;
import rx.Observable;
import rx.functions.Func1;

public class DataManager {

    private final AmaysimExamService mAmaysimExamService;
    private final PreferencesHelper mPreferencesHelper;
    private final AssetsHelper mAssetsHelper;

    @Inject
    public DataManager(AmaysimExamService watchTowerService,
                       PreferencesHelper preferencesHelper, AssetsHelper assetsHelper) {
        mAmaysimExamService = watchTowerService;
        mPreferencesHelper = preferencesHelper;
        mAssetsHelper = assetsHelper;
    }

    public PreferencesHelper getPreferencesHelper() {
        return mPreferencesHelper;
    }

    public AssetsHelper getAssetsHelper() {
        return mAssetsHelper;
    }

    public Observable<List<Character>> getCharacters(int[] ids) {
        List<Integer> characterIds = new ArrayList<>(ids.length);
        for (int id : ids) {
            characterIds.add(id);
        }
        return Observable.from(characterIds).concatMap(new Func1<Integer, Observable<Character>>() {
            @Override
            public Observable<Character> call(Integer integer) {
                return mAmaysimExamService.getCharacter(integer);
            }
        }).toList();
    }

    public Collection getCollection() {
        return mAssetsHelper.getCollection();
    }

    public String getSalutation() {
        return mAssetsHelper.getSalutation();
    }

    public List<Included> getIncludedList() {
        return mAssetsHelper.getIncludedList();
    }

    public String getMsn() {
        return mAssetsHelper.getMsn();
    }

    public Datum getAccount() {
        return mAssetsHelper.getAccount();
    }
}
