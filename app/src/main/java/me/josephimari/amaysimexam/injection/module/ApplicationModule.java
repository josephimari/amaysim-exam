package me.josephimari.amaysimexam.injection.module;

import android.app.Application;
import android.content.Context;

import me.josephimari.amaysimexam.data.remote.AmaysimExamService;
import me.josephimari.amaysimexam.injection.ApplicationContext;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * Provide application-level dependencies. Mainly singleton object that can be injected from
 * anywhere in the app.
 */
@Module
public class ApplicationModule {
    protected final Application mApplication;

    public ApplicationModule(Application application) {
        mApplication = application;
    }

    @Provides
    Application provideApplication() {
        return mApplication;
    }

    @Provides
    @ApplicationContext
    Context provideContext() {
        return mApplication;
    }

    @Provides
    @Singleton
    AmaysimExamService provideAndroidBoilerplateService() {
        return AmaysimExamService.Factory.makeAndroidBoilerplateService(mApplication);
    }

}