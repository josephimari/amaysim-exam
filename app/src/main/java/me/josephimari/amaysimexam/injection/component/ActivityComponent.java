package me.josephimari.amaysimexam.injection.component;

import android.app.LauncherActivity;

import me.josephimari.amaysimexam.injection.PerActivity;
import me.josephimari.amaysimexam.injection.module.ActivityModule;
import me.josephimari.amaysimexam.ui.activity.AccountActivity;
import me.josephimari.amaysimexam.ui.activity.HomeActivity;
import me.josephimari.amaysimexam.ui.activity.LoginActivity;
import me.josephimari.amaysimexam.ui.activity.SplashActivity;

import dagger.Component;

@PerActivity
@Component(dependencies = ApplicationComponent.class, modules = ActivityModule.class)
public interface ActivityComponent {

    void inject(LauncherActivity launcherActivity);
    void inject(HomeActivity homeActivity);
    void inject(LoginActivity loginActivity);
    void inject(SplashActivity splashActivity);
    void inject(AccountActivity accountActivity);

}

