package me.josephimari.amaysimexam.injection.component;

import android.app.Application;
import android.content.Context;

import me.josephimari.amaysimexam.AmaysimExamApplication;
import me.josephimari.amaysimexam.data.DataManager;
import me.josephimari.amaysimexam.data.local.PreferencesHelper;
import me.josephimari.amaysimexam.data.remote.AmaysimExamService;
import me.josephimari.amaysimexam.injection.ApplicationContext;
import me.josephimari.amaysimexam.injection.module.ApplicationModule;

import javax.inject.Singleton;

import dagger.Component;

@Singleton
@Component(modules = ApplicationModule.class)
public interface ApplicationComponent {

    void inject(AmaysimExamApplication amaysimExamApplication);

    @ApplicationContext
    Context context();
    Application application();
    AmaysimExamService androidBoilerplateService();
    PreferencesHelper preferencesHelper();
    DataManager dataManager();

}
