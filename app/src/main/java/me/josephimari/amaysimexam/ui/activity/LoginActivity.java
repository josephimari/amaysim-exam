package me.josephimari.amaysimexam.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.nineoldandroids.animation.Animator;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import butterknife.OnClick;
import me.josephimari.amaysimexam.R;
import me.josephimari.amaysimexam.data.DataManager;

public class LoginActivity extends BaseActivity {

    @Inject
    DataManager mDataManager;

    @Bind(R.id.text_msn)
    AutoCompleteTextView msnText;

    @Bind(R.id.input_msn)
    View msnContainer;

    @Bind(R.id.button_login)
    View loginButton;

    @Bind(R.id.image_logo)
    ImageView logoImage;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, LoginActivity.class);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_login);
        ButterKnife.bind(this);
    }

    @OnClick(R.id.button_login)
    void login(View view) {
        String input = msnText.getText().toString();

        if (input.isEmpty()) {
            msnText.setError("Please provide MSN");
            return;
        }

        if (input.equals(mDataManager.getMsn())) {
            ActivityOptionsCompat options = ActivityOptionsCompat.
                    makeSceneTransitionAnimation(this, logoImage, "logo");
            startActivity(SplashActivity.getStartIntent(LoginActivity.this), options.toBundle());
        } else {
            msnText.setError("MSN provided has no match.");
        }

    }
}
