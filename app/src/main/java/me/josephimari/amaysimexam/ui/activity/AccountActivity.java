package me.josephimari.amaysimexam.ui.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.josephimari.amaysimexam.R;
import me.josephimari.amaysimexam.data.DataManager;
import me.josephimari.amaysimexam.data.model.Attribute;
import me.josephimari.amaysimexam.data.model.Datum;

public class AccountActivity extends BaseActivity {

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Inject
    DataManager mDataManager;

    @Bind(R.id.text_salutation)
    TextView salutationText;

    @Bind(R.id.text_birthdate)
    TextView birthdateText;

    @Bind(R.id.text_contact_nunmber)
    TextView contactNumberText;

    @Bind(R.id.text_email)
    TextView emailText;

    @Bind(R.id.text_verified)
    TextView verifiedText;

    @Bind(R.id.text_subscribed)
    TextView subscribedText;

    @Bind(R.id.text_payment_type)
    TextView paymentTypeText;

    @Bind(R.id.text_unbilled_charges)
    TextView unbilledChargesText;

    @Bind(R.id.text_next_billing_date)
    TextView nextBillingDateText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_account);
        ButterKnife.bind(this);
        setupToolbar();
        setupView();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setTitle("");
        }
    }

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, AccountActivity.class);
        return intent;
    }

    private void setupView() {
        Datum account = mDataManager.getAccount();
        if (account == null || account.attributes == null) {
            Toast.makeText(this, "Something went wrong with information acquired", Toast.LENGTH_LONG).show();
            finish();
        } else {
            Attribute attribute = account.attributes;
            if (mDataManager.getSalutation() == null) {
                salutationText.setVisibility(View.GONE);
            } else {
                salutationText.setText(mDataManager.getSalutation());
            }

            if (attribute.dateOfBirth == null) {
                birthdateText.setVisibility(View.GONE);
            } else {
                birthdateText.setText(attribute.dateOfBirth);
            }

            if (attribute.contactNumber == null) {
                contactNumberText.setVisibility(View.GONE);
            } else {
                contactNumberText.setText(attribute.contactNumber);
            }

            emailText.setText(attribute.emailAddress == null ? "n/a" : attribute.emailAddress);
            verifiedText.setText(attribute.emailAddressVerified ? "verified" : "unverified");
            subscribedText.setText(attribute.emailSubscriptionStatus ? "subscribed" : "unsubscribed");
            paymentTypeText.setText(attribute.paymentType == null ? "n/a" : attribute.paymentType);
            unbilledChargesText.setText(attribute.unbilledCharges == null ? "n/a" : String.format("$%.2f", attribute.unbilledCharges));
            nextBillingDateText.setText(attribute.nextBillingDate == null ? "n/a" : attribute.nextBillingDate);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                supportFinishAfterTransition();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
