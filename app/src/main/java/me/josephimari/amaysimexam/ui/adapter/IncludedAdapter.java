package me.josephimari.amaysimexam.ui.adapter;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.josephimari.amaysimexam.R;
import me.josephimari.amaysimexam.data.model.Attribute;
import me.josephimari.amaysimexam.data.model.Included;

/**
 * Created by Joseph on 1/31/2017.
 */

public class IncludedAdapter extends RecyclerView.Adapter<IncludedAdapter.IncludedHolder> {

    List<Included> mIncludedList;
    private static final int SERVICE_TYPE = 0;
    private static final int SUBSCRIPTION_TYPE = 1;
    private static final int PRODUCT_TYPE = 2;

    @Inject
    public IncludedAdapter() {
        this.mIncludedList = new ArrayList<>();
    }

    @Override
    public IncludedAdapter.IncludedHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        int layout = R.layout.item_service;
        switch (viewType) {
            case SERVICE_TYPE:
                layout = R.layout.item_service;
                break;
            case SUBSCRIPTION_TYPE:
                layout = R.layout.item_subscription;
                break;
            case PRODUCT_TYPE:
                layout = R.layout.item_product;
                break;
            default:
                break;
        }

        View view =
                LayoutInflater.from(parent.getContext())
                        .inflate(layout, parent, false);
        return new IncludedAdapter.IncludedHolder(view);
    }

    @Override
    public void onBindViewHolder(final IncludedHolder holder, final int position) {
        final Context context = holder.itemView.getContext();
        final Included included = mIncludedList.get(position);
        final int viewType = getItemViewType(position);

        final Attribute attribute = included.attributes;
        if (attribute == null) return;

        switch (viewType) {
            case SERVICE_TYPE:

                holder.msnContainer.setVisibility(attribute.msn == null ? View.GONE : View.VISIBLE);
                holder.msnText.setText(attribute.msn);

                if (attribute.credit == null) {
                    holder.creditContainer.setVisibility(View.GONE);
                } else {
                    holder.creditContainer.setVisibility(View.VISIBLE);
                    holder.creditText.setText(String.format("%.2f GB", attribute.credit.doubleValue() / 1024));
                }

                holder.creditExpiryContainer.setVisibility(attribute.creditExpiry == null ? View.GONE : View.VISIBLE);
                holder.creditExpiryText.setText(attribute.creditExpiry);

                holder.thresholdContainer.setVisibility(attribute.dataUsageThreshold ? View.VISIBLE : View.GONE);

                break;
            case SUBSCRIPTION_TYPE:

                if (attribute.includedDataBalance == null) {
                    holder.dataBalanceContainer.setVisibility(View.GONE);
                } else {
                    holder.dataBalanceContainer.setVisibility(View.VISIBLE);
                    holder.dataBalanceText.setText(String.format("%.2f GB", attribute.includedDataBalance.doubleValue() / 1024));
                }

                if (attribute.includedCreditBalance == null) {
                    holder.creditBalanceContainer.setVisibility(View.GONE);
                } else {
                    holder.creditBalanceContainer.setVisibility(View.VISIBLE);
                    holder.creditBalanceText.setText(String.format("%.2f GB", attribute.includedCreditBalance.doubleValue() / 1024));
                }

                if (attribute.includedRolloverCreditBalance == null) {
                    holder.rollOverCreditBalanceContainer.setVisibility(View.GONE);
                } else {
                    holder.rollOverCreditBalanceContainer.setVisibility(View.VISIBLE);
                    holder.rolloverCreditBalanceText.setText(String.format("%.2f GB", attribute.includedRolloverCreditBalance.doubleValue() / 1024));
                }

                if (attribute.includedRolloverDataBalance == null) {
                    holder.rollOverDataBalanceContainer.setVisibility(View.GONE);
                } else {
                    holder.rollOverDataBalanceContainer.setVisibility(View.VISIBLE);
                    holder.rolloverDataBalanceText.setText(String.format("%.2f GB", attribute.includedRolloverDataBalance.doubleValue() / 1024));
                }

                if (attribute.includedInternationalTalkBalance == null) {
                    holder.internationalTalkBalanceContainer.setVisibility(View.GONE);
                } else {
                    holder.internationalTalkBalanceContainer.setVisibility(View.VISIBLE);
                    holder.internationalTalkBalanceText.setText(String.format("%.2f GB", attribute.includedInternationalTalkBalance.doubleValue() / 1024));
                }

                holder.expiryDateContainer.setVisibility(attribute.expiryDate == null ? View.GONE : View.VISIBLE);
                holder.expiryDateText.setText(attribute.expiryDate);

                holder.autoRenewalContainer.setVisibility(attribute.autoRenewal ? View.VISIBLE : View.GONE);
                holder.primarySubscriptionContainer.setVisibility(attribute.primarySubscription ? View.VISIBLE : View.GONE);

                break;
            case PRODUCT_TYPE:

                holder.nameContainer.setVisibility(attribute.name == null ? View.GONE : View.VISIBLE);
                holder.nameText.setText(attribute.name);

                if (attribute.includedData == null) {
                    holder.includedDataContainer.setVisibility(View.GONE);
                } else {
                    holder.includedDataContainer.setVisibility(View.VISIBLE);
                    holder.includedDataText.setText(String.format("%.2f GB", attribute.includedData.doubleValue() / 1024));
                }

                if (attribute.includedCredit == null) {
                    holder.includedCreditContainer.setVisibility(View.GONE);
                } else {
                    holder.includedCreditContainer.setVisibility(View.VISIBLE);
                    holder.includedCreditText.setText(String.format("%.2f GB", attribute.includedCredit.doubleValue() / 1024));
                }

                if (attribute.includedInternationTalk == null) {
                    holder.includedInternationalTalkContainer.setVisibility(View.GONE);
                } else {
                    holder.includedInternationalTalkContainer.setVisibility(View.VISIBLE);
                    holder.includedInternationalTalkText.setText(String.format("%.2f GB", attribute.includedInternationTalk.doubleValue() / 1024));
                }

                holder.unlimitedTextContainer.setVisibility(attribute.unlimitedText ? View.VISIBLE : View.GONE);
                holder.unlimitedTalkContainer.setVisibility(attribute.unlimitedTalk ? View.VISIBLE : View.GONE);
                holder.unlimitedInternationalTextContainer.setVisibility(attribute.unlimitedInternationalText ? View.VISIBLE : View.GONE);
                holder.unlimitedInternationalTalkContainer.setVisibility(attribute.unlimitedInternationalTalk ? View.VISIBLE : View.GONE);

                holder.priceContainer.setVisibility(attribute.price == null ? View.VISIBLE : View.GONE);
                holder.priceText.setText(String.format("$%.2f", attribute.price));
                break;
            default:
                break;
        }

    }

    @Override
    public int getItemCount() {
        return mIncludedList.size();
    }

    @Override
    public int getItemViewType(int position) {

        String type = mIncludedList.get(position).type;
        if (type == null) return 0;
        if (type.equals("services")) return SERVICE_TYPE;
        if (type.equals("subscriptions")) return SUBSCRIPTION_TYPE;
        if (type.equals("products")) return PRODUCT_TYPE;

        return 0;
    }

    public void setIncludedList(List<Included> includedList) {
        mIncludedList = includedList;
        notifyDataSetChanged();
    }

    class IncludedHolder extends RecyclerView.ViewHolder {

        @Nullable
        @Bind(R.id.container_msn)
        View msnContainer;

        @Nullable
        @Bind(R.id.text_msn)
        TextView msnText;

        @Nullable
        @Bind(R.id.container_credit)
        View creditContainer;

        @Nullable
        @Bind(R.id.text_credit)
        TextView creditText;

        @Nullable
        @Bind(R.id.container_credit_expiry)
        View creditExpiryContainer;

        @Nullable
        @Bind(R.id.text_credit_expiry)
        TextView creditExpiryText;

        @Nullable
        @Bind(R.id.container_threshold)
        View thresholdContainer;

        @Nullable
        @Bind(R.id.container_data_balance)
        View dataBalanceContainer;

        @Nullable
        @Bind(R.id.text_data_balance)
        TextView dataBalanceText;

        @Nullable
        @Bind(R.id.container_credit_balance)
        View creditBalanceContainer;

        @Nullable
        @Bind(R.id.text_credit_balance)
        TextView creditBalanceText;

        @Nullable
        @Bind(R.id.container_rollover_credit_balance)
        View rollOverCreditBalanceContainer;

        @Nullable
        @Bind(R.id.text_rollover_credit_balance)
        TextView rolloverCreditBalanceText;

        @Nullable
        @Bind(R.id.container_rollover_data_balance)
        View rollOverDataBalanceContainer;

        @Nullable
        @Bind(R.id.text_rollover_data_balance)
        TextView rolloverDataBalanceText;

        @Nullable
        @Bind(R.id.container_international_talk_balance)
        View internationalTalkBalanceContainer;

        @Nullable
        @Bind(R.id.text_international_talk_balance)
        TextView internationalTalkBalanceText;


        @Nullable
        @Bind(R.id.container_expiry_date)
        View expiryDateContainer;

        @Nullable
        @Bind(R.id.text_expiry_date)
        TextView expiryDateText;

        @Nullable
        @Bind(R.id.container_auto_renewal)
        View autoRenewalContainer;

        @Nullable
        @Bind(R.id.container_primary_subscription)
        View primarySubscriptionContainer;

        @Nullable
        @Bind(R.id.container_name)
        View nameContainer;

        @Nullable
        @Bind(R.id.text_name)
        TextView nameText;

        @Nullable
        @Bind(R.id.container_included_data)
        View includedDataContainer;

        @Nullable
        @Bind(R.id.text_included_data)
        TextView includedDataText;

        @Nullable
        @Bind(R.id.container_included_credit)
        View includedCreditContainer;

        @Nullable
        @Bind(R.id.text_included_credit)
        TextView includedCreditText;

        @Nullable
        @Bind(R.id.container_included_international_talk)
        View includedInternationalTalkContainer;

        @Nullable
        @Bind(R.id.text_included_international_talk)
        TextView includedInternationalTalkText;

        @Nullable
        @Bind(R.id.container_unlimited_text)
        View unlimitedTextContainer;

        @Nullable
        @Bind(R.id.container_unlimited_talk)
        View unlimitedTalkContainer;

        @Nullable
        @Bind(R.id.container_unlimited_international_text)
        View unlimitedInternationalTextContainer;

        @Nullable
        @Bind(R.id.container_unlimited_international_talk)
        View unlimitedInternationalTalkContainer;

        @Nullable
        @Bind(R.id.container_price)
        View priceContainer;

        @Nullable
        @Bind(R.id.text_price)
        TextView priceText;

        public IncludedHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }


}
