package me.josephimari.amaysimexam.ui.activity;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import butterknife.Bind;
import butterknife.ButterKnife;
import me.josephimari.amaysimexam.R;
import me.josephimari.amaysimexam.data.DataManager;
import me.josephimari.amaysimexam.data.model.Included;
import me.josephimari.amaysimexam.ui.adapter.IncludedAdapter;

public class HomeActivity extends BaseActivity implements LoaderManager.LoaderCallbacks<List<Included>> {

    private static final String TAG = HomeActivity.class.getSimpleName();

    @Bind(R.id.recycler_included)
    RecyclerView mIncludedRecycler;

    @Bind(R.id.toolbar)
    Toolbar mToolbar;

    @Inject
    DataManager mDataManager;

    @Inject
    IncludedAdapter mIncludedAdapter;

    public static Intent getStartIntent(Context context) {
        Intent intent = new Intent(context, HomeActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
        return intent;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityComponent().inject(this);
        setContentView(R.layout.activity_home);
        ButterKnife.bind(this);
        setupToolbar();
        setupRecyclerView();
        initLoader();
    }

    private void setupToolbar() {
        setSupportActionBar(mToolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setTitle(mDataManager.getSalutation());
            setTransitionName();
        }
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    private void setTransitionName() {
        mToolbar.getChildAt(0).setTransitionName("salutation");
    }

    private void setupRecyclerView() {
        mIncludedRecycler.setLayoutManager(new LinearLayoutManager(this));
        mIncludedRecycler.setAdapter(mIncludedAdapter);
    }

    private void initLoader() {
        getSupportLoaderManager().initLoader(TAG.hashCode(), null, this).forceLoad();
    }


    @Override
    public Loader<List<Included>> onCreateLoader(int id, Bundle args) {
        return new AsyncTaskLoader<List<Included>>(this) {
            @Override
            public List<Included> loadInBackground() {
                List<Included> makes = mDataManager.getIncludedList();
                return makes;
            }
        };

    }

    @Override
    public void onLoadFinished(Loader<List<Included>> loader, List<Included> data) {
        mIncludedAdapter.setIncludedList(data);
    }

    @Override
    public void onLoaderReset(Loader<List<Included>> loader) {
        mIncludedAdapter.setIncludedList(new ArrayList<Included>(0));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_view_profile:
                ActivityOptionsCompat options = ActivityOptionsCompat.
                        makeSceneTransitionAnimation(this, mToolbar.getChildAt(0), "salutation");
                startActivity(AccountActivity.getStartIntent(this), options.toBundle());
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
