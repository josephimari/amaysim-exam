package me.josephimari.amaysimexam;

import me.josephimari.amaysimexam.data.DataManager;
import me.josephimari.amaysimexam.data.local.AssetsHelper;
import me.josephimari.amaysimexam.data.local.PreferencesHelper;
import me.josephimari.amaysimexam.data.model.Character;
import me.josephimari.amaysimexam.data.remote.AmaysimExamService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.List;

import rx.Observable;
import rx.observers.TestSubscriber;

import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DataManagerTest {

    @Mock
    AmaysimExamService mMockAmaysimExamService;
    @Mock
    PreferencesHelper mMockPreferencesHelper;
    @Mock
    AssetsHelper mMockAssetsHelper;

    DataManager mDataManager;

    @Before
    public void setUp() {
        mDataManager = new DataManager(mMockAmaysimExamService, mMockPreferencesHelper, mMockAssetsHelper);
    }

    @Test
    public void shouldLoadCharacters() throws Exception {
        int[] ids = new int[]{10034, 14050, 10435, 35093};
        List<Character> characters = MockModelFabric.makeListOfMockCharacters(4);
        for (int i = 0; i < ids.length; i++) {
            when(mMockAmaysimExamService.getCharacter(ids[i]))
                    .thenReturn(Observable.just(characters.get(i)));
        }

        TestSubscriber<List<Character>> result = new TestSubscriber<>();
        mDataManager.getCharacters(ids).subscribe(result);
        result.assertNoErrors();
        result.assertValue(characters);
    }

}
